FROM alpine:3.8

EXPOSE 5000

RUN apk add --no-cache python3 python3-dev

RUN python3 -m ensurepip && \
rm -r /usr/lib/python*/ensurepip && \
    pip3 install --upgrade pip setuptools && \
    if [ ! -e /usr/bin/pip ]; then ln -s pip3 /usr/bin/pip ; fi && \
    if [[ ! -e /usr/bin/python ]]; then ln -sf /usr/bin/python3 /usr/bin/python; fi && \
    rm -r /root/.cache

WORKDIR /opt/clubes-api/

RUN mkdir app tests
COPY app ./app
COPY tests ./tests
COPY requirements.txt . 
RUN pip install -r requirements.txt


CMD ["python", "app/main.py"]

